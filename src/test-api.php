<?php

require_once 'Mandrill.php';

try {

    $mandrill = new Mandrill('your-api-key-here');

    $mandrill->useProxy('url-to-your-proxy-here')->sslVerifyHost(0)->sslVerifyPeer(0);

    $fetcher = Mandrill_Fetcher::load($mandrill->users->info());

    print_r($fetcher->get('stats.today'));

} catch (Mandrill_Error $e) {
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
}

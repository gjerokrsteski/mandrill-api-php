<?php

/**
 * A well-known object that other objects can use to find common objects and services.
 */
class Mandrill_Fetcher
{
    /**
     * The temporary storage for the accumulator.
     *
     * @var \ArrayObject
     */
    protected $battery;

    /**
     * @param array $result
     */
    public function __construct(array $result)
    {
        $this->battery = new ArrayObject($result, ArrayObject::STD_PROP_LIST + ArrayObject::ARRAY_AS_PROPS);
    }

    /**
     * @param array $result

     * @return Mandrill_Fetcher
     */
    public static function load(array $result)
    {
        return new self($result);
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param string|integer $index The index or identifier.
     * @param mixed          $default
     *
     * @return mixed|null
     */
    public function get($index, $default = null)
    {
        if ($this->battery->offsetExists($index)) {
            return $this->battery->offsetGet($index);
        }
        $array = $this->battery->getArrayCopy();
        foreach ((array)explode('.', $index) as $segment) {
            if (!is_array($array) || !array_key_exists($segment, $array)) {
                return $default;
            }
            $array = $array[$segment];
        }

        return $array;
    }
}
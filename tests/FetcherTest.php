<?php

class FetcherTest extends PHPUnit_Framework_TestCase
{

    public function testSettingItems()
    {
        $this->assertInstanceOf('Mandrill_Fetcher', Mandrill_Fetcher::load(array()));
    }

    public function testGettingItemsStatically()
    {
        $fetcher = Mandrill_Fetcher::load(array('list' => array(1, 2, 3)));

        $this->assertEquals(array(1, 2, 3), $fetcher->get('list'));
    }

    public function testGettingItemsIfNotAtBatteryBecauseNoOverride()
    {
        $fetcher  = Mandrill_Fetcher::load(array('list' => array(1, 2, 3)));
        $fetcher2 = Mandrill_Fetcher::load(array('list-two' => array(4, 5, 6)));

        $this->assertEquals(array(1, 2, 3), $fetcher->get('list'), 'problems on fetcher');
        $this->assertNull($fetcher2->get('list'), 'problems on fetcher2 getting list');
        $this->assertEquals(array(4, 5, 6), $fetcher2->get('list-two'), 'problems on fetchers getting list-two');
    }

    public function testFetchingSegments()
    {
        $fetcher = Mandrill_Fetcher::load(array('list' => array('of' => array('numbers' => array(1, 2, 3)))));

        $this->assertEquals(array(1, 2, 3), $fetcher->get('list.of.numbers'));
    }

}
